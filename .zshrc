# ------------------------------------------------------------------------------
# 0. Lines configured by zsh-newuser-install
# ------------------------------------------------------------------------------

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory beep nomatch notify
unsetopt autocd extendedglob
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/gtz/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# ------------------------------------------------------------------------------
# 1. Environment configuration
# ------------------------------------------------------------------------------

source ~/zsh-git-prompt/zshrc.sh
# an example prompt
#PROMPT='%B%m%~%b$(git_super_status) %# '
PS1="%F{}┌─%f%B[%n@%m]%b─(%1~)
└─[%T]$"

# ------------------------------------------------------------------------------
# 2. Aliases
# ------------------------------------------------------------------------------

alias cp='cp -iv'
alias mv='mv -iv'
alias ll='ls -FGlAhp'
alias cd..='cd ../'			# for fast typing

alias ..='cd ..'			# go up 1 dir
alias ...='cd ../..'			# go up 2 dirs
alias .3='cd ../../../'			# go up 3 dirs
alias .4='cd ../../../../'		# go up 4 dirs
alias .5='cd ../../../../../'		# go up 5 dirs
alias .6='cd ../../../../../../'	# go up 6 dirs
